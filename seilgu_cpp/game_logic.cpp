#include "game_logic.h"
#include "protocol.h"
#include <ctime>
#include <ratio>
#include <chrono>
#include "util.h"

#include <stack>
#include <map>

#define PI 3.14159265358979

#define ENABLE_TURBO_RAM

using namespace hwo_protocol;


namespace gameinfo {
	int turboDurationTicks = 0;
	double  turboFactor = 3.0;
	std::vector<double> lanes_dist;
	std::vector<segment> pieces;
	int tick0, tick1, tick2;
	int va_enabled = 0;
	double k1 = 0.2;
	double k2 = 0.02;
	double A = 0.5303;
	double B = 0.00125;
	jsoncons::json data0, data1, data2;
	piece_len piecelen;
	piece_rad piecerad;
	std::vector<int> directions;
	std::vector<car> cars0, cars1, cars2;

	double throttle2 = 0;
	double throttle1 = 0;
	double throttle0 = 0;

	bool k12_det = false;
	bool torque_det = false;

	bool carNearby = false;
	std::string mycarName;

	bool lockon = false;
	
}

using namespace gameinfo;

inline int clamp(int id, int range) {
	if (id >= range) return range-1;
	else if (id < 0) return 0;
	else return id;
}

int getCar(std::string name) {
	for (int i=0; i<cars0.size(); i++) {
		if (cars0[i].name == name)
			return i;
	}
	return -1;
}

game_logic::game_logic() : action_map {
	{ "gameInit", &game_logic::on_game_init }, 
	{ "yourCar", &game_logic::on_your_car }, 
	{ "join", &game_logic::on_join },
	{ "gameStart", &game_logic::on_game_start },
	{ "carPositions", &game_logic::on_car_positions },
	{ "crash", &game_logic::on_crash },
	{ "spawn", &game_logic::on_spawn },
	{ "gameEnd", &game_logic::on_game_end },
	{ "error", &game_logic::on_error }, 
	{ "turboAvailable", &game_logic::on_turbo_available }, 
	{ "joinRace", &game_logic::on_join_race },
	{ "turboStart", &game_logic::on_turbo_start },
	{ "turboEnd", &game_logic::on_turbo_end }, 
	{ "finish", &game_logic::on_finish }, 
	{ "dnf", &game_logic::on_dnf }, 
}{}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg) {
	//std::cout << "REACT" << msg << std::endl;
	if (!msg.has_member("msgType") || !msg.has_member("data")) 		return { make_ping() };

	if (msg.has_member("gameTick"))	{
		int t = msg["gameTick"].as<int>();
		if (t > tick0) {
			tick2 = tick1;
			tick1 = tick0;
			tick0 = t;

			throttle2 = throttle1;
			throttle1 = throttle0;

		}
	}

	const auto& msg_type = msg["msgType"].as<std::string>();
	const auto& data = msg["data"];
	auto action_it = action_map.find(msg_type);

	if (action_it != action_map.end()) {
		return (action_it->second)(this, data);
	} else {
		std::cout << "Unknown message type: " << msg_type << std::endl;
	}

	return { make_ping() };
}

game_logic::msg_vector game_logic::on_finish(const jsoncons::json& data) {
	if (!data.has_member("name")) return { make_ping() };
	for (auto c : cars0) 
		if (c.name == data["name"].as<std::string>()) 	c.finishedRace = true;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_join_race(const jsoncons::json& data) {
	std::cout << "Using joinRace:" << data << std::endl;
	mycarName = data["botId"]["name"].as<std::string>();
	std::cout << "My car is : " << mycarName << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data) {
	gameinfo::turboDurationTicks = data["turboDurationTicks"].as<int>();
	gameinfo::turboFactor = data["turboFactor"].as<double>();

	int mycar_id = getCar(mycarName);
	if (mycar_id != -1 && cars0[mycar_id].crashing == false && cars0[mycar_id].finishedRace == false) {
		if (cars0[mycar_id].turboAvailable == 0) cars0[mycar_id].turboAvailable = 1;

		printf("\tgameinfo :: turboAvailable Duration = %d, turboFactor = %f\n", gameinfo::turboDurationTicks, gameinfo::turboFactor);
		printf("\tTurbos Remaining : %d\n", cars0[mycar_id].turboAvailable);	
	}

	return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_start(const jsoncons::json& data) {
	#ifdef DDEBUG
		std::cout << data << std::endl;
	#endif
	int cid = getCar(data["name"].as<std::string>());
	if (cid != -1 && cars0[cid].crashing == false && cars0[cid].finishedRace == false) {
		cars0[cid].onTurbo = true;
		cars0[cid].turboBeginTick = tick0;
		cars0[cid].turboAvailable = 0;

		if (data["name"].as<std::string>() == mycarName) {
			printf("\tZZZZzooooooom~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("\tturboAvailable : %d\n", cars0[cid].turboAvailable);
		}
	}


	return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_end(const jsoncons::json& data) {
	int cid = getCar(data["name"].as<std::string>());
	if (cid != -1) cars0[cid].onTurbo = false;
	return { make_ping() };
}

bool is_out_of_rail(const car& car) {
	return car.finishedRace || car.crashing || car.onDNF;
}

void plan_directions(car source, car target) {
	if (source.p == target.p && 
		source.startLane == target.startLane &&
		source.endLane == target.endLane && 
		target.x > source.x) return;

	// find a path through lanes to the target
	while (source.endLane != target.startLane || source.p != target.p) {
		source.startLane = source.endLane;
		source.p = (source.p + 1) % pieces.size();
		if (source.endLane != target.startLane && pieces[source.p].switchable == true) {
			directions[source.p] = ( target.startLane > source.endLane ? 1 : -1 );
			source.endLane += directions[source.p];
		}
	}
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data) {
	std::cout << data << std::endl;

	// clean all the global variables
	bool build_data = false;
	if (!data["race"]["raceSession"].has_member("quickRace") ||
       std::tolower(data["race"]["raceSession"]["quickRace"].as<std::string>()[0]) == 't'
      ) { // qual round or quickRace
		build_data = true;
	}

	va_enabled = 0;

	cars0.clear();
	cars2.clear();
	cars1.clear();
	tick0 = -1;
	tick1 = -1;
	tick2 = -1;
	throttle0 = 0;
	throttle1 = 0;
	lockon = false;

	if (build_data == true) {
		lanes_dist.clear();
		pieces.clear();
		directions.clear();
		piecelen.clean();
		piecerad.clean();

		// building segment data
		const jsoncons::json& pcs = data["race"]["track"]["pieces"];
		for (int i=0; i<pcs.size(); i++) {
			segment seg;
			if (pcs[i].has_member("length")) {
				seg.type = LINE;
				seg.length = pcs[i]["length"].as<double>();
				seg.radius = 1.0/0.0;
			} else {
				seg.type = CURVE;
				seg.angle = pcs[i]["angle"].as<double>();
				seg.radius = pcs[i]["radius"].as<double>();
			}

			seg.switchable = pcs[i].has_member("switch");

			gameinfo::pieces.push_back(seg);
		}

		const jsoncons::json& lns = data["race"]["track"]["lanes"];
		int nlanes = lns.size();
		for (int j=0; j<nlanes; j++) {
			for (int k=0; k<nlanes; k++) {
				if (lns[k]["index"] == j) {
					gameinfo::lanes_dist.push_back(lns[k]["distanceFromCenter"].as<double>());
					break;
				}
			}
		}

		// piecerad
		piecerad.init(pieces.size(), nlanes);
		for (int i=0; i<pieces.size(); i++) {
			for (int j=0; j<lanes_dist.size(); j++) {
				x_rad xr;
				xr.x = 0;
				xr.rad = (pieces[i].radius + lanes_dist[j]*(pieces[i].angle > 0 ? -1 : 1))
					* (pieces[i].angle > 0 ? 1 : -1); // signed radius
				piecerad.pushRad(i, j, j, xr);	// dummy radius for same lane
				// different lane gives undefined radius in interpolation
			}
		}

		// piecelen
		piecelen.init(pieces.size(), nlanes);
		for (int i=0; i<pieces.size(); i++) {
			for (int j=0; j<lanes_dist.size(); j++) {
				double distj = ( pieces[i].type == LINE ? pieces[i].length : 
					( pieces[i].angle*PI/180.0*piecerad.getRad(i, j, j, 0) ) );	// we already have same-lane radius
				piecelen.setLen(i, j, j, distj);	// same lane curved length
				piecelen.setMeasured(i, j, j, true);

				if (pieces[i].switchable == false) continue;

				if (j+1 < lanes_dist.size()) {
					double distjplus = ( pieces[i].type == LINE ? pieces[i].length : 
						( pieces[i].angle*PI/180.0*piecerad.getRad(i, j+1, j+1, 0) ) );
					piecelen.setLen(i, j, j+1, fmax(distj, distjplus)+3);	// different lane using max of two lanes
					piecelen.setLen(i, j+1, j, fmax(distj, distjplus)+3);
					piecelen.setMeasured(i, j, j+1, false);
					piecelen.setMeasured(i, j+1, j, false);
				}
				if (j-1 >= 0) {
					double distjminus = ( pieces[i].type == LINE ? pieces[i].length : 
						( pieces[i].angle*PI/180.0*piecerad.getRad(i, j-1, j-1, 0) ) );
					piecelen.setLen(i, j, j-1, fmax(distj, distjminus)+3);
					piecelen.setLen(i, j-1, j, fmax(distj, distjminus)+3);
					piecelen.setMeasured(i, j, j-1, false);
					piecelen.setMeasured(i, j-1, j, false);
				}
			}
		}

		// directions
		directions.resize(pieces.size());
		for (auto &i : directions)
			i = 0;

	}

	const jsoncons::json& cardata = data["race"]["cars"];
	std::cout << "Number of cars0 : " << cardata.size() << std::endl;

	for (int i=0; i<cardata.size(); i++) {
		car cc;
		cc.name = cardata[i]["id"]["name"].as<std::string>();
		cc.length = cardata[i]["dimensions"]["length"].as<double>();
		cc.width = cardata[i]["dimensions"]["width"].as<double>();
		cc.crashing = false;
		cc.onTurbo = false;
		cc.onDNF = false;
		cc.lastCrashedTick = -1000;
		cc.finishedRace = false;
		cc.turboBeginTick = -1000;
		cc.turboAvailable = 0;
		cc.p = 0;
		cc.x = 0;
		cc.startLane = 0;
		cc.endLane = 0;
		cc.angle = 0;
		cc.v = 0;
		cc.w = 0;
		cc.laps = 0;
		cc.lastThrottle = 0.0;
		cars0.push_back(cc);
	}

	return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data) {
	mycarName = data["name"].as<std::string>();
	std::cout << "My car is : " << mycarName << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data) {
	std::cout << "on_joined" << std::endl;
	mycarName = data["name"].as<std::string>();
	std::cout << "My car is : " << mycarName << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data) {
	std::cout << "on_game_start" << std::endl;
	return { make_ping() };
}

 // To be replaced by it's content
inline double getDistance(int p, int sLane, int eLane) {
	return piecelen.getLen(p, sLane, eLane);
}

void update_one_step(car& ic, double throttle) {
	if (ic.tick - ic.turboBeginTick > turboDurationTicks || ic.tick < ic.turboBeginTick) ic.onTurbo = false;
	else ic.onTurbo = true;

	double radius = fabs(piecerad.getRad(ic.p, ic.startLane, ic.endLane, ic.x));
	double alpha = - B*ic.v*ic.angle - 0.1*ic.w;
	double torque = 0;
	if (pieces[ic.p].type == CURVE && ic.v > (B*240.0/A)*sqrt(radius)) {
		if (pieces[ic.p].angle > 0) 	torque = A/sqrt(radius)*ic.v*ic.v - B*240.0*ic.v;
		else							torque = - A/sqrt(radius)*ic.v*ic.v + B*240.0*ic.v;
	}
	alpha += torque;
	ic.w += alpha;
	ic.angle += ic.w;

	double a;
	if (ic.onTurbo) 		a = k1*throttle*turboFactor - k2*ic.v;
	else					a = k1*throttle - k2*ic.v;
	ic.v += a;
	ic.x += ic.v;

	if (ic.x > getDistance(ic.p, ic.startLane, ic.endLane)) {
		ic.x -= getDistance(ic.p, ic.startLane, ic.endLane);
		ic.p = (ic.p + 1) % pieces.size();
		
		if (ic.p == 0) ic.laps++;

		ic.startLane = ic.endLane;
		ic.endLane = clamp(ic.startLane + directions[ic.p], lanes_dist.size());
	}

	ic.lastThrottle = throttle;
	ic.tick++;
}

// dist from source car to target car, source car should be behind target car
double distToCar(car source, car target) {
	if (source.p == target.p && 
		source.startLane == target.startLane &&
		source.endLane == target.endLane && 
		target.x > source.x) return target.x - source.x;

	// find a path through lanes to the target
	double dist = -source.x;
	do {
		dist += piecelen.getLen(source.p, source.startLane, source.endLane);
		source.startLane = source.endLane;
		source.p = (source.p + 1) % pieces.size();
		if (source.endLane != target.startLane && pieces[source.p].switchable == true)
			source.endLane += ( target.startLane > source.endLane ? 1 : -1 );		
	}while (source.startLane != target.startLane || source.p != target.p);
	dist += target.x;

	return dist;
}
// Global variable-independent version of predict function, use to simulate under some condition
// curr- prefix to represent local varialbe

bool mustCrash(car ic) {
	const int NSTEP = 100;
	for (int i = 0; i<NSTEP; ++i) {
		if (fabs(ic.angle) > 60.0)	return true;
		update_one_step(ic, 0.0);
	}
	return false;
}

double predict(car ic) {
	const int NSTEP = 100;

	car ictemp = ic;
	double throttle;
	for (int i=0; i<NSTEP; i++) {
		if (i <= 0) throttle = 1.0;
		else throttle = 0.0;
		
		car next_ic = ictemp; update_one_step(next_ic, ictemp.lastThrottle);
		if (next_ic.p != ictemp.p && directions[next_ic.p] != 0 && clamp(ictemp.endLane + directions[next_ic.p], lanes_dist.size()) != ictemp.endLane) // switching lane
			throttle = ictemp.lastThrottle;

		update_one_step(ictemp, throttle);

		if (fabs(ictemp.angle) > 60.0) return 0.0;
	}

	return 1.0;

}

void set_fastest_directions(car ic) {
	while (pieces[ic.p].switchable == false) {
		update_one_step(ic, predict(ic));
	}

	int bestlane = ic.startLane;
	int mintick = ic.tick + 50000;
	double resx = 0;

	for (int lane = ic.startLane - 1; lane <= ic.startLane + 1; lane++) {
		if (lane < 0 || lane >= lanes_dist.size()) continue;
		car temp = ic; temp.endLane = lane;

		while (temp.p == ic.p || pieces[temp.p].switchable == false) {
			update_one_step(temp, predict(temp));
		}

		if (temp.tick < mintick
			|| (temp.tick == mintick && temp.x > resx) ) {
			mintick = temp.tick;
			bestlane = lane;
			resx = temp.x;
		}
	}

	if (bestlane > ic.startLane) {
		directions[ic.p] = 1;
	} else if (bestlane < ic.startLane) {
		directions[ic.p] = -1;
	} else {
		directions[ic.p] = 0;
	}


	/*for (int this_piece = 0; this_piece < pieces.size(); this_piece++) {
		if (pieces[this_piece].switchable) {
			// check between this switchable and next switchable, which side is shorter
			int j;
			for (j=1; j<(int)pieces.size(); j++)
				if (pieces[(this_piece+j)%pieces.size()].switchable) break;

			double delta_dist = 0;
			for (int i=1; i<j; i++) {
				int next_piece = (this_piece+i)%pieces.size();
				if (pieces[next_piece].type == CURVE) {
					delta_dist += pieces[next_piece].angle * pieces[next_piece].radius;
				}
			}

			if (delta_dist > 0.0001) 		{ directions[this_piece] = 1;	}
			else if (delta_dist < -0.0001) 	{ directions[this_piece] = -1; }
			else 							{ directions[this_piece] = 0; }
		}
		else {
			directions[this_piece] = 0;
		}
	}*/
}

int evaluateTurbo(car ic) {
	const int NSTEP = 100;

	car control = ic;
	car test = ic;
	test.onTurbo = true;
	test.turboBeginTick = ic.tick+1;
	double dist0 = 0;
	for (int i=0; i<NSTEP; i++) {
		update_one_step( test, predict(test) );
		update_one_step( control, predict(control) );
		dist0 += test.v - control.v;
	}

//	const int NTRY = 60;
	std::vector<int> trysteps = { 0, 1, 2, 4, 8, 16, 32, 64, 96, 128, 160, 192, 224, 256, 288, 320 };
	car ictemp = ic;
	for (int i=1; i<trysteps.size(); i++) {
		//for (int k=0; k<10; k++)
		//	update_one_step( ictemp, predict(ictemp) );
		for (int k=trysteps[i-1]; k<trysteps[i]; k++)
			update_one_step( ictemp, predict(ictemp) );

		car control = ictemp;
		car test = ictemp;
		test.onTurbo = true;
		test.turboBeginTick = ictemp.tick+1;

		double dist = 0;
		for (int j=0; j<NSTEP; j++) {
			update_one_step( test, predict(test) );
			update_one_step( control, predict(control) );
			dist += test.v - control.v;
		}

		if (dist > dist0) return 0;
	}

	return 1;

	/*for (int maxth = 0; maxth < turboDurationTicks + 1; maxth++) {
		if (mustCrash(ic)) return maxth;
		update_one_step(ic, 1.0);
	}

	return turboDurationTicks;*/
}

/**
 * Ram
 */

bool canRam(car rammer, car rammee, int& hit_countdown, bool useTurbo) {
	if (useTurbo == true) {
		rammer.onTurbo = true;
		rammer.turboBeginTick = rammer.tick;
	}

	const int NSTEP = 200;
	for (int i = 0; i<NSTEP; ++i) {
		double rammee_throttle = predict(rammee);
		update_one_step(rammee, rammee_throttle);
		if (fabs(rammee.angle) > 60.0) return false;

		if (pieces[rammee.p].switchable == true) return false;

		update_one_step(rammer, 1.0);
		if (fabs(rammer.angle) > 60.0) return false;

		double dist = distToCar(rammer, rammee);

		// Check collision
		if (fabs(dist) < 0.25*(rammer.length + rammee.length)) {
			// switch speed
			double tmp_v = rammer.v;
			rammer.v = rammee.v;
			rammee.v = tmp_v;

			// if target will crash, and mine won't, then GO!
			if (!mustCrash(rammer) && mustCrash(rammee)) {
				hit_countdown = i;
				return true;
			} else {
				return false;
			}
		}
	}

	return false;
}
// === Ram logic end

 // doesn't update turbo or default values, so it won't get modified.
void make_car(car &cc, const jsoncons::json& cData, std::vector<car> &pCar, int carid, int tick) {
	car pc = pCar[carid];
	cc.p = cData[carid]["piecePosition"]["pieceIndex"].as<int>();
	cc.x = cData[carid]["piecePosition"]["inPieceDistance"].as<double>();
	cc.angle = cData[carid]["angle"].as<double>();

	if (tick - pc.tick == 2) { // missing tick HACK: only carPositions might fall into this.
		car pred = pc; update_one_step(pred, throttle2); car prev = pred; update_one_step(pred, throttle1);
		cc.v = pred.v;
		cc.w = pred.w;
		cc.lastThrottle = (cc.v - prev.v + k2*prev.v)/k1;
	} else {
		cc.v = cc.x - pc.x;
		if (cc.v < 0.0) {
			cc.v += getDistance(pc.p, pc.startLane, pc.endLane);
		}
		cc.w = cc.angle - pc.angle;
		cc.lastThrottle = (cc.v - pc.v + k2*pc.v)/k1;
	}

	cc.startLane = cData[carid]["piecePosition"]["lane"]["startLaneIndex"].as<int>();
	cc.endLane = cData[carid]["piecePosition"]["lane"]["endLaneIndex"].as<int>();
	cc.laps = cData[carid]["piecePosition"]["lap"].as<int>();

	cc.tick = tick;
}

void correction_len() {
	// can't use other cars0 because we don't know their throttle
	int mycar_id = getCar(mycarName);
	if (mycar_id == -1) { printf("Cannot find myself. (correction())\n"); exit(0); }
	car mycar = cars0[mycar_id];

	// first use my_car's default values : crashing, turbos, then udpate the rest
	car prev_car = mycar; make_car(prev_car, data1, cars2, mycar_id, tick1);
	if (prev_car.startLane == prev_car.endLane) return;

	car curr_car = prev_car; update_one_step(curr_car, throttle1);

	std::cout << '{' << mycar.p << ", " << curr_car.p << '}' << std::endl;
	if (mycar.p == curr_car.p && curr_car.p == prev_car.p) // nothing to correct
		return;

	double len = piecelen.getLen(prev_car.p, prev_car.startLane, prev_car.endLane);

	if (mycar.p == prev_car.p) { // predict changed piece, but actually not => piece is at least new x long
		piecelen.setLen(prev_car.p, prev_car.startLane, prev_car.endLane, mycar.x);
	}
	else if (curr_car.p == prev_car.p) { // mycarp.p != prev_car.p // prediction didn't change but actually changed, reshorten the difference
		piecelen.setLen(prev_car.p, prev_car.startLane, prev_car.endLane, curr_car.x - mycar.x);
		piecelen.setMeasured(prev_car.p, prev_car.startLane, prev_car.endLane, true);
	}
	else { 
		// both unequal
		piecelen.setLen(prev_car.p, prev_car.startLane, prev_car.endLane, len + curr_car.x - mycar.x);
		piecelen.setMeasured(prev_car.p, prev_car.startLane, prev_car.endLane, true);
	}

	double newlen = piecelen.getLen(prev_car.p, prev_car.startLane, prev_car.endLane);

	std::cout << "newlen " << prev_car.p << "(" << prev_car.startLane << ',' << prev_car.endLane << ")" <<
		":" << len << "=>" << newlen << std::endl;
		
}

void correction_rad() {

	int mycar_id = getCar(mycarName);
	if (mycar_id == -1) { printf("Cannot find myself. (correction())\n"); exit(0); }
	car mycar = cars0[mycar_id];

	{
		int p0 = data0[mycar_id]["piecePosition"]["pieceIndex"].as<int>();
		int p1 = data1[mycar_id]["piecePosition"]["pieceIndex"].as<int>();
		int p2 = data2[mycar_id]["piecePosition"]["pieceIndex"].as<int>();
		if (p0 != p1 || p1 != p2) return;
	}

	car prev_car = mycar; make_car(prev_car, data1, cars2, mycar_id, tick1);
	if (prev_car.startLane == prev_car.endLane) return; // same lane unecessary

	car curr_car = prev_car; update_one_step(curr_car, throttle1);

	double last_rad;
	if (pieces[prev_car.p].type == LINE) return;

	double torque = mycar.angle - prev_car.angle - prev_car.w + B*prev_car.v*prev_car.angle + 0.1*prev_car.w;
	if (fabs(torque) < 0.001) return; // centrifugal force less than threshold, no conclusion
	
	if (pieces[prev_car.p].angle > 0) {
		last_rad = (torque + B*240.0*prev_car.v)/(prev_car.v*prev_car.v*A);
		last_rad = 1.0/(last_rad*last_rad);
	} else {
		last_rad = (torque - B*240.0*prev_car.v)/(prev_car.v*prev_car.v*A);
		last_rad = -1.0/(last_rad*last_rad);
	}
	printf("R=% 5.2f ", last_rad);
	// seems ok enough
	if (fabs(last_rad - piecerad.getRad(prev_car.p, prev_car.startLane, prev_car.endLane, prev_car.x)) > 0.001) {
		x_rad rp;
		rp.x = prev_car.x;
		rp.rad = last_rad;
		piecerad.pushRad(prev_car.p, prev_car.startLane, prev_car.endLane, rp);

		std::cout << "newrad " << prev_car.p << "(" << prev_car.startLane << ',' << prev_car.endLane << ")" <<
		"=" << "(x=" << rp.x << ", r=" << rp.rad << ')' << std::endl;
	}
}
	
double jam_speed(int startPiece, int endPiece, int lane) {
	int nPieces = endPiece - startPiece;
	if (nPieces < 0) nPieces += pieces.size();
	double minspeed = 1e10;
	for (int i=0; i<nPieces+1; i++) {
		for (auto cc : cars0) {
			if (is_out_of_rail(cc)) continue;
			if (cc.p == (startPiece+i)%pieces.size() && cc.endLane == lane) {
				if (cc.v > -0.001 && cc.v < minspeed) minspeed = cc.v;
			}
		}
	}
	return minspeed;
}

int overtake_direction(car cc) { // cc is car before entering switchable piece
	int startPiece = (cc.p+1)%pieces.size();
	int endPiece = (startPiece+1)%pieces.size();
	while (pieces[endPiece].switchable == false) { endPiece = (endPiece + 1)%pieces.size(); }
	endPiece = (endPiece + pieces.size() - 1) % pieces.size();

	// check if original direciton is good enough
	int bestlane = clamp(cc.endLane + directions[startPiece], lanes_dist.size());
	double bestspeed = jam_speed( startPiece, endPiece, bestlane );
	if ( bestspeed > 1e5 || bestspeed > cc.v ) return directions[startPiece];

	int minlane = clamp(cc.endLane-1, lanes_dist.size());
	int maxlane = clamp(cc.endLane+1, lanes_dist.size());
	for (int lane = minlane; lane <= maxlane; lane++) {
		double jamspeed = jam_speed(startPiece, endPiece, lane);
		if (jamspeed > bestspeed) {
			bestspeed = jamspeed;
			bestlane = lane;
		}
	}
	if (bestlane == cc.endLane) return 0;
	else if (bestlane > cc.endLane) return 1;
	else return -1;
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data) {
	//std::cout << data << std::endl;
	if (pieces.size() == 0) { std::cout << "NO ON_GAME_INIT!!" << std::endl; exit(0); }
	int mycar_id = getCar(mycarName);
	if (mycar_id == -1) { printf("Cannot find myself.\n"); exit(0); }
	
	data2 = data1;
	data1 = data0;
	data0 = data;

	// build cars info 
	// don't change crashing, turbo, etc. so crash, on_turbo doesn't get erased
	cars2 = cars1;
	cars1 = cars0;
	for (int i=0; i<cars0.size(); i++) {
		make_car(cars0[i], data0, cars1, i, tick0);
	}

	if (va_enabled < 10) va_enabled++;
	if (va_enabled < 3) return { make_throttle(1.0) };

	car mycar = cars0[mycar_id];
	if (mycar.crashing == true) {
		if (tick0 - mycar.lastCrashedTick > 400) 	mycar.crashing = false;
		else										return { make_ping() };
	}
	if (mycar.finishedRace == true) 	return { make_ping() };

	/*if (mycar.onTurbo == true) {
		if (tick0 - mycar.turboBeginTick > turboDurationTicks)
			mycar.onTurbo = false;
	}*/

	carNearby = false;
	for (int i = 0; i<cars0.size(); ++i) {
		if (i == mycar_id) continue;
		
		car cc = cars0[i];
		const double nearbyDist = 0.25*(cars0[i].length + cars0[mycar_id].length);
		std::cout << "dist1:" << distToCar(mycar, cc) << std::endl; //<< " dist2:" << distToCar(cc, mycar) << std::endl;
		if (distToCar(mycar, cc) < nearbyDist || distToCar(cc, mycar) < nearbyDist) {
			carNearby = true; break;
		}
	}

	if (k12_det && carNearby == false && tick0 - tick1 == 1) 										correction_len();
	if (k12_det && torque_det && carNearby == false && tick0 - tick1 == 1 && tick1 - tick2 == 1) 	correction_rad();
	//printf("carNearBy = %s\n", carNearby?"true":"false");


	if (k12_det == false) {
		static double v[3];
		static int cont_tick = 0;
		static double lastk1 = k1;
		static double lastk2 = k2;
		double thisk1 = 0;
		double thisk2 = 0;

		if (carNearby == false && tick0 - tick1 == 1 && mycar.v > 0) {
			v[cont_tick] = mycar.v;
			cont_tick++;
		}
		else
			cont_tick = 0;

		if (cont_tick > 2) {
			double a[2];
			for (int i=0; i<2; i++) {
				a[i] = v[i+1] - v[i];
			}
			thisk2 = -(a[1]-a[0])/(v[1]-v[0]);
			thisk1 = (a[1] + thisk2*v[1])/1.0;

			if (fabs( (lastk1 - thisk1) / lastk1 ) < 0.001 ) {
				k1 = thisk1;
				k2 = thisk2;
				printf("k1 = %f\n", k1);
				printf("k2 = %f\n", k2);
				k12_det = true;
				printf("k determined.\n");
			} else {
				cont_tick--;
				for (int i=0; i<2; i++) {
					v[i] = v[i+1];
				}
			}

			lastk1 = thisk1;
			lastk2 = thisk2;
		}
	}
	if (k12_det == false) {
		double throttle_ = 1.0;
		std::cout << mycar.p << " [" << mycar.x << ',' << mycar.angle << "]";
		car pred_car = mycar; update_one_step(pred_car, throttle_);
		std::cout << "-->[" << pred_car.x << ',' << pred_car.angle << "]" << std::endl;
		return { make_throttle(throttle_) };
	}

	static bool calib_speed_det = false;
	static double calib_speed = 20.0;

	if (k12_det == true && torque_det == false) {
		static double lastA = A;
		static double lastB = B;
		double thisA = 0;
		double thisB = 0;
		static double angle[3], w[3], v[3], radius[3];
		static int cont_tick = 0;

		if (carNearby == false && tick0 - tick1 == 1 && fabs(mycar.angle) > 0.001 && pieces[mycar.p].type == CURVE
			&& fabs(mycar.v - calib_speed) < 0.0001) {
			angle[cont_tick] = mycar.angle;
			w[cont_tick] = mycar.w;
			v[cont_tick] = mycar.v;
			radius[cont_tick] = fabs(piecerad.getRad(mycar.p, mycar.startLane, mycar.endLane, mycar.x));
			cont_tick++;
		} else {
			cont_tick = 0;
		}
		
		if (cont_tick > 2) { // successfully collect 3 data on theta, w
			double alpha[2];
			for (int i=0; i<2; i++)		alpha[i] = w[i+1] - w[i];

			double R[2];
			for (int i=0; i<2; i++) 	R[i] = alpha[i] + 0.1*w[i];

			thisB = -(R[1] - R[0]) / (v[1]*angle[1] - v[0]*angle[0]);

			if (pieces[mycar.p].angle > 0) {
				thisA = (R[0] + thisB*240.0*v[0] + thisB*v[0]*angle[0])*sqrt(radius[0])/(v[0]*v[0]);
				printf("A = %g\n", thisA);
				printf("B = %g\n", thisB);
			}
			else {
				thisA = -(R[0] - thisB*240.0*v[0] + thisB*v[0]*angle[0])*sqrt(radius[0])/(v[0]*v[0]);
				printf("A = %g\n", thisA);
				printf("B = %g\n", thisB);
			}

			if (fabs( (lastA - thisA)/lastA ) < 0.001 ) {
				A = thisA;
				B = thisB;
				torque_det = true;
				printf("A determined = %f.\n", A);
				printf("B determined = %f.\n", B);
			}
			else {

				for (int i=0; i<2; i++) {
					w[i] = w[i+1];
					angle[i] = angle[i+1];
					v[i] = v[i+1];
					radius[i] = radius[i+1];
				}
				cont_tick--;
			}

			lastA = thisA;
			lastB = thisB;
		}
	}
	if (k12_det == true && torque_det == false) {
		if (pieces[mycar.p].type == CURVE && fabs(mycar.angle) > 0.001 && calib_speed_det == false) {
			calib_speed = mycar.v;
			calib_speed_det = true;
		} else if (pieces[mycar.p].type == LINE) {
			calib_speed_det = false;
			calib_speed = 20.0;
		}
		double throttle_ = (calib_speed - mycar.v + k2*mycar.v) / k1;
		if (throttle_ > 1.0) throttle_ = 1.0;
		else if (throttle_ < 0.0) throttle_ = 0.0;

		std::cout << mycar.p << " [" << mycar.x << ',' << mycar.angle << "]";
		car pred_car = mycar; update_one_step(pred_car, throttle_);
		std::cout << "-->[" << pred_car.x << ',' << pred_car.angle << "]" << std::endl;

		return { make_throttle(throttle_) };
	}

	// Test ram function.
	#if 1
	
	do {
		int rank = 1;
		int target_id = mycar_id;
		double mindist = 1e10;
		double minid = mycar_id;
		for (int i=0; i<cars0.size(); i++) {
			if (i == mycar_id) continue;
			car cc = cars0[i];
			if (is_out_of_rail(cc)) continue;

			double dist = distToCar(mycar, cc);
			if (dist < mindist) {
				mindist = dist;
				minid = i;
			}

			if (cc.laps > mycar.laps) {
				target_id = i;
				rank++;
			}
			else if (cc.laps == mycar.laps && cc.p > mycar.p) {
				target_id = i;
				rank++;
			}
		}

		if (rank != 2 || target_id != minid || target_id == mycar_id) {
			lockon = false; // somebody is between us
		} else {
			lockon = true;	
		}

		if (lockon == true) {
			// plan directions
			car target = cars0[target_id];
			plan_directions(mycar, target);

			int hit_in_tick = -1;
			bool can_ram_noturbo = canRam(mycar, target, hit_in_tick, false);
			if (can_ram_noturbo) {
				std::cout << "RAMMING! Est # ticks = " << hit_in_tick << std::endl;
				return { make_throttle(1.0) };
			} else if (mycar.onTurbo == false && mycar.turboAvailable > 0) {
				hit_in_tick = -1;
				bool can_ram_turbo = canRam(mycar, target, hit_in_tick, true);
				car test = mycar; update_one_step(test, throttle1);
				if (!mustCrash(test) && can_ram_turbo) {
					std::cout << "TURBO RAMMING! MAKE_TURBO! Est # ticks = " << hit_in_tick << std::endl;
					return { make_turbo() };
				}
			}
		}
	}while(0);
	#endif

	/** Lane Switching **/
	#if 1
	if (torque_det == true) {
		car temp = mycar; update_one_step(temp, throttle1);
		if (temp.p != mycar.p && pieces[temp.p].switchable == true) { // switching time

			set_fastest_directions(mycar);

			if (lockon == false) {
				directions[temp.p] = overtake_direction(mycar);
			}

			car pred_car = mycar; update_one_step(pred_car, throttle1);

			if (!mustCrash(pred_car) && clamp(mycar.endLane + directions[pred_car.p], lanes_dist.size()) != mycar.endLane) {
				int dir = directions[temp.p];
					return { make_lane(dir) };	
			}
		}
	}
	#endif
	
	/** Turbo **/
	#if 1
	if (mycar.onTurbo == false && mycar.turboAvailable > 0) {
		car test = mycar; update_one_step(test, throttle1);
		if (!mustCrash(test) && evaluateTurbo(mycar)) {
			return { make_turbo() };
		}
		/*int turboCap = evaluateTurbo(mycar);
		printf("\tturboCap = %d", turboCap);
		if (turboCap >= 17) {
			car temp = mycar;
			update_one_step(temp, throttle1);
			if (!mustCrash(temp))		return { make_turbo() };
		}*/
	}
	#endif

	/** Throttle **/
	double throttle = predict(mycar);
	if (throttle > 1.0) throttle = 1.0;
	else if (throttle < 0.0) throttle = 0.0;

	{
		std::cout << "[[" << tick0 << "]] " << mycar.p << " [" << mycar.x << ',' << mycar.angle << "]";
		car pred_car = mycar; update_one_step(pred_car, throttle);
		std::cout << "-->[" << pred_car.x << ',' << pred_car.angle << "]" << std::endl;
		printf(" < %4.2f > ", throttle); //std::cout << " <" << throttle << "> ";
		if (mycar.turboAvailable > 0) std::cout << "hasTurbo";
		if (mycar.onTurbo) std::cout << "onTurbo";
		std::cout << std::endl;
	}

	return { make_throttle( throttle ) };

}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data) {
	std::cout << data["name"] << " crashed" << std::endl;
	int cid = getCar(data["name"].as<std::string>());
	if (cid != -1) {
		cars0[cid].lastCrashedTick = tick0;
		cars0[cid].crashing = true;
		cars0[cid].turboAvailable = 0;
		if (cid == getCar(mycarName)) {  // spec says the throttle will be cleared as 0
			throttle0 = 0;
		}
	}
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data) {
	std::cout << data["name"] << " spawned" << std::endl;
	int cid = getCar(data["name"].as<std::string>());
	if (cid != -1) {
		cars0[cid].crashing = false;
	}
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_dnf(const jsoncons::json& data) {
	std::cout << data["car"]["name"] << " dnf" << std::endl;
	int cid = getCar(data["car"]["name"].as<std::string>());
	if (cid != -1) {
		cars0[cid].onDNF = true;
	}
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data) {
	std::cout << "Race ended" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data) {
	std::cout << "Error: " << data.to_string() << std::endl;
	return { make_ping() };
}
