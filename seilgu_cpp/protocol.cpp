#include "protocol.h"
#include "game_logic.h"

using gameinfo::tick0;
using gameinfo::throttle0;
using gameinfo::throttle1;

namespace hwo_protocol
{

  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data, bool useTick = true)
  {
    jsoncons::json r;
    r["msgType"] = msg_type;
    r["data"] = data;
    if (useTick)
      r["gameTick"] = tick0;
    return r;
  }

  jsoncons::json make_create(const std::string& name, const std::string& key, const std::string& trackName, const std::string& password, int car_count) 
  {
    jsoncons::json data;
    jsoncons::json botId;
    botId["name"] = name;
    botId["key"] = key;
    data["botId"] = botId;
    data["trackName"] = trackName;
    data["password"] = password;
    data["carCount"] = car_count;
    return make_request("createRace", data);
  }

  jsoncons::json make_join_race(const std::string& name, const std::string& key, const std::string& trackName, const std::string& password, int car_count) 
  {
    jsoncons::json data;
    jsoncons::json botId;
    botId["name"] = name;
    botId["key"] = key;
    data["botId"] = botId;
    data["trackName"] = trackName;
    data["password"] = password;
    data["carCount"] = car_count;
    return make_request("joinRace", data);
  }

  jsoncons::json make_join(const std::string& name, const std::string& key, const std::string& trackName)
  {
    jsoncons::json data;
    jsoncons::json botId;
    botId["name"] = name;
    botId["key"] = key;
    data["botId"] = botId;
    data["trackName"] = trackName;
    data["carCount"] = 1;
    return make_request("joinRace", data);
  }

  /*jsoncons::json make_join(const std::string& name, const std::string& key)
  {
    jsoncons::json data;
    jsoncons::json botId;
    botId["name"] = name;
    botId["key"] = key;
    data["botId"] = botId;
    data["carCount"] = 1;
    return make_request("joinRace", data);
  }*/

  
  jsoncons::json make_join(const std::string& name, const std::string& key)
  {
    jsoncons::json data;
    data["name"] = name;
    data["key"] = key;
    return make_request("join", data);
  }

  jsoncons::json make_ping()
  {
    printf("\tmake ping.\n");
    return make_request("ping", jsoncons::null_type(), false);
  }

  jsoncons::json make_throttle(double throttle)
  {
    printf("\tmake throttle %3.1f.\n", throttle);
    throttle0 = throttle;
    return make_request("throttle", throttle);
  }

  jsoncons::json make_turbo() {
    printf("\tmake turbo.\n");
    return make_request("turbo", "zzz");
  }

  jsoncons::json make_lane(int direction) {
    printf("\tmake lane %d.\n", direction);
    if (direction == 1)
      return make_request("switchLane", "Right");
    else if (direction == -1)
      return make_request("switchLane", "Left");
  }

}  // namespace hwo_protocol
