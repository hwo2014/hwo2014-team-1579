#ifndef HWO_PROTOCOL_H
#define HWO_PROTOCOL_H

#include <string>
#include <iostream>
#include <jsoncons/json.hpp>

namespace hwo_protocol
{
  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data, bool useTick);
  jsoncons::json make_create(const std::string& name, const std::string& key, const std::string& trackName, const std::string& password, int car_count);
  jsoncons::json make_join_race(const std::string& name, const std::string& key, const std::string& trackName, const std::string& password, int car_count);
  jsoncons::json make_join(const std::string& name, const std::string& key, const std::string& trackName);
  jsoncons::json make_join(const std::string& name, const std::string& key);
  jsoncons::json make_ping();
  jsoncons::json make_throttle(double throttle);
  jsoncons::json make_turbo();
  jsoncons::json make_lane(int direction);
}

#endif
