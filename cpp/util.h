
#ifndef HWO_UTIL_H
#define HWO_UTIL_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>
#include <time.h>

class Clock {
private:
	timespec t1, t2;
public:
	int gets();
	int getns();
	void start();
	void stop();
	timespec timediff(timespec sta, timespec end);
};

bool find_A(double &A, jsoncons::json& data0, jsoncons::json& data1, jsoncons::json& data2);
bool find_k(double &k1, double &k2, jsoncons::json& data0, jsoncons::json& data1, jsoncons::json& data2);

#endif