#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>


enum segment_type {LINE, CURVE};

typedef struct _segment {
	segment_type type;
	double length;
	double angle, radius;
	bool switchable;
}segment;

struct x_rad {
	double x;
	double rad;
};

struct piece_rad {
	std::vector<x_rad> *r;
	int np, nl;
	piece_rad() { r = 0; }
	~piece_rad() { clean(); }
	void clean() {
		if (r) delete[] r;
		r = 0;
	}
	void init(int nump, int numl) {
		clean();
		nl = numl;
		np = nump;
		r = new std::vector<x_rad>[np*nl*nl];
	}
	void pushRad(int p, int sl, int el, x_rad xr) {
		r[p*nl*nl + sl*nl + el].push_back(xr);
	}
	double getRad(int p, int sl, int el, double x) {
		int segsize = r[p*nl*nl + sl*nl + el].size();
		if (segsize == 0) {
			double ssr = r[p*nl*nl + sl*nl + sl][0].rad; 
			double eer = r[p*nl*nl + el*nl + el][0].rad;
			if (ssr > 0) 	return fmin(ssr, eer);
			else			return fmax(ssr, eer);
		}

		double mindist = 1e10;
		int mini = 0;
		for (int i=0; i<segsize; i++) {
			double dist = fabs(x - r[p*nl*nl + sl*nl + el][i].x);
			if (dist < mindist) {
				mindist = dist;
				mini = i;
			}
		}
		return r[p*nl*nl + sl*nl + el][mini].rad;
	}
};

struct piece_len {
	double *d;
	bool	*measured;
	int np, nl;
	piece_len() { d = 0; measured = 0; }
	~piece_len() { clean(); }
	void clean() {
		if (d) delete[] d;
		d = 0;
		if (measured) delete[] measured;
		measured = 0;
	}
	void init(int nump, int numl) {
		clean();
		nl = numl;
		np = nump;
		d = new double[np*nl*nl];
		measured = new bool[np*nl*nl];
	}
	bool getMeasured(int p, int sl, int el) {
		return measured[p*nl*nl + sl*nl + el];
	}
	void setMeasured(int p, int sl, int el, bool m) {
		measured[p*nl*nl + sl*nl + el] = m;
	}
	void setLen(int p, int sl, int el, double v) {
		d[p*nl*nl + sl*nl + el] = v;
	}
	double getLen(int p, int sl, int el) {
		return d[p*nl*nl + sl*nl + el];
	}
};

typedef struct _car {
	bool crashing = false;
	bool onTurbo = false;
	bool onDNF = false;
	int lastCrashedTick = -1000;
	bool finishedRace = false;
	double turboBeginTick = -1000;
	int turboAvailable = 0;

	double length, width;
	int p;
	double x;
	int startLane, endLane;
	double angle;

	int tick;
	int laps = 0;

	double v, w;

	double lastThrottle;

	std::string name;
}car;

namespace gameinfo {
	extern int turboDurationTicks;
	extern double  turboFactor;
	extern std::vector<double> lanes_dist;
	extern std::vector<segment> pieces;
	extern int tick0, tick1;

	extern int va_enabled;
	extern double k1;
	extern double k2;
	extern double A;
	extern jsoncons::json data0, data1, data2;
	extern piece_len piecelen;
	extern piece_rad piecerad;
	extern std::vector<int> directions;
	extern std::vector<car> cars0, cars1, cars2;

	extern double throttle0;
	extern double throttle1;

	extern std::string mycarName;
	extern bool carNearby;
}

class game_logic
{
public:
	typedef std::vector<jsoncons::json> msg_vector;

	game_logic();
	msg_vector react(const jsoncons::json& msg);

private:
	typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
	const std::map<std::string, action_fun> action_map;

	msg_vector on_game_init(const jsoncons::json& data);
	msg_vector on_your_car(const jsoncons::json& data);
	msg_vector on_finish(const jsoncons::json& data);
	msg_vector on_join(const jsoncons::json& data);
	msg_vector on_game_start(const jsoncons::json& data);
	msg_vector on_car_positions(const jsoncons::json& data);
	msg_vector on_crash(const jsoncons::json& data);
	msg_vector on_spawn(const jsoncons::json& data);
	msg_vector on_game_end(const jsoncons::json& data);
	msg_vector on_error(const jsoncons::json& data);
	msg_vector on_turbo_available(const jsoncons::json& data);
	msg_vector on_turbo_start(const jsoncons::json& data);
	msg_vector on_turbo_end(const jsoncons::json& data);
	msg_vector on_join_race(const jsoncons::json& data);
	msg_vector on_dnf(const jsoncons::json& data);

	msg_vector get_action();
};

bool mustCrash(car ic);

inline double getDistance(int pId, int sLane, int eLane);

#endif
