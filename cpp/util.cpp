
#include "util.h"

void Clock::start() {
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
}
void Clock::stop() {
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t2);
}
int Clock::gets() {
	return timediff(t1, t2).tv_sec;
}
int Clock::getns() {
	return timediff(t1, t2).tv_nsec;
}

timespec Clock::timediff(timespec sta, timespec end) {
	timespec temp;
	if ((end.tv_nsec-sta.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec-sta.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-sta.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec-sta.tv_sec;
		temp.tv_nsec = end.tv_nsec-sta.tv_nsec;
	}
	return temp;
}

bool find_A(double &A, jsoncons::json& data0, jsoncons::json& data1, jsoncons::json& data2) {
	return false;
}

bool find_k(double &k1, double &k2, jsoncons::json& data0, jsoncons::json& data1, jsoncons::json& data2) {
	return false;
}