#include "game_logic.h"
#include "protocol.h"
#include <ctime>
#include <ratio>
#include <chrono>
#include <cmath>

#include "bot.h"
#include "components.h"

using namespace hwo_protocol;

game_logic::game_logic()
	: action_map
{
	{ "gameInit", &game_logic::on_game_init }, 
		{ "yourCar", &game_logic::on_your_car }, 
		{ "join", &game_logic::on_join },
		{ "gameStart", &game_logic::on_game_start },
		{ "carPositions", &game_logic::on_car_positions },
		{ "crash", &game_logic::on_crash },
		{ "gameEnd", &game_logic::on_game_end },
		{ "error", &game_logic::on_error }
}
{
}

// TODO using static variables are dangerous, fix this later
int Race::tick = 0;
Track* Race::track = new Track();
Cars* Race::cars = new Cars();

Bot* Race::bot = new pipibjc();

/**
 * Game logic implementation
 */
game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
	//std::cout << msg << std::endl;
	const auto& msg_type = msg["msgType"].as<std::string>();
	const auto& data = msg["data"];
	auto action_it = action_map.find(msg_type);
	if (action_it != action_map.end())
	{
		// record tick;
		if (msg.has_member("gameTick")) {
			// status.setTick(msg["gameTick"].as<int>());
			Race::tick = msg["gameTick"].as<int>();
		}

		return (action_it->second)(this, data);
	}
	else
	{
		std::cout << "Unknown message type: " << msg_type << std::endl;
		return { make_ping() };
	}
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
	std::cout << "Game init : track : " << data["race"]["track"]["id"] << std::endl;
	Race::track->setTrack(data["race"]["track"]);
	Race::cars->setCars(data["race"]["cars"]);

	Race::bot->on_game_init();

	return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
	Race::cars->setYourCar(data);

	return { make_ping() };
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
	std::cout << "Joined" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
	std::cout << "Race started" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	Race::cars->updateCarPositions(data);
	jsoncons::json ret = Race::bot->on_car_positions();

	std::cout << "React : " << ret << std::endl;
	return { ret };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	std::cout << data["name"] << " crashed" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
	std::cout << "Race ended" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
	std::cout << "Error: " << data.to_string() << std::endl;
	return { make_ping() };
}
